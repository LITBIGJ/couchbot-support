.. _creatorsettings:

===============
Adding Creators
===============

.. note:: These commands are toggled, if you want to remove someone from the list, run the command again.

+--------------+-------------------------------------------------+-------------------------------------------+
| Name         | Example                                         | Usage                                     |
+--------------+-------------------------------------------------+-------------------------------------------+
| mixer        | ``!cb mixer MattTheDev #DiscordChannel``        | Adds a creator to your list.              |
+--------------+-------------------------------------------------+-------------------------------------------+
| picarto      | ``!cb picarto MattTheDev #DiscordChannel``      | Adds a creator to your list.              |
+--------------+-------------------------------------------------+-------------------------------------------+
| twitch       | ``!cb twitch MattTheDev #DiscordChannel``       | Adds a creator to your list.              |
+--------------+-------------------------------------------------+-------------------------------------------+
| youtube      | ``!cb youtube UC24569877 #DiscordChannel vod``  | Adds a creator (VOD) to your list.        |
+--------------+-------------------------------------------------+-------------------------------------------+
| youtube      | ``!cb youtube UC24569877 #DiscordChannel live`` | Adds a creator (Live) to your list.       |
+--------------+-------------------------------------------------+-------------------------------------------+
| youtube      | ``!cb youtube UC24569877 #DiscordChannel both`` | Adds a creator (VOD & Live) to your list. |
+--------------+-------------------------------------------------+-------------------------------------------+
| creator list | ``!cb creator list``                            | Lists all creators added to the server.   |
+--------------+-------------------------------------------------+-------------------------------------------+

If you wish to add a personal touch when adding specific creators you can do this by specifying a message to go along with the announcement.

.. note:: You can use **@everyone** and **@here** when creating the custom messages!

The first command adds a Mixer streamer called MattTheDev to announce in a channel called #DiscordChannel.
You can also see that it has some basic Discord formatting and will ping @everyone.

The second command edits the announcement for MattTheDev by replacing the message and changing the role that gets pinged.

The third command if run after the first or second, will remove the announcement from the list of creators and not announce in the future.

.. code-block:: none

	!cb mixer MattTheDev #DiscordChannel "The Developer of CouchBot has gone **LIVE**, come watch @everyone"
	!cb mixer MattTheDev #DiscordChannel "Come watch Matt play @here"
	!cb mixer MattTheDev #DiscordChannel

.. important:: The following variables are available to use in the **Live** and **Published** messages.
.. code-block:: none

    %TITLE%
    %GAME% - Not working for YouTube
    %CHANNEL%
    %URL%
