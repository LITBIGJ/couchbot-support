.. _allowconfig:

===================
Allow Configuration
===================

Use the following commands to configure what the bot is allowed to do.
Some of these such as ``!cb allow live`` are the basic commands to get the bot setup.

+------------------+--------------------------+----------------------------------------------------------------+
| Name             | Example                  | Usage                                                          |
+------------------+--------------------------+----------------------------------------------------------------+
| allow live       | ``!cb allow live``       | Toggles allowing stream announces for live streamers.          |
+------------------+--------------------------+----------------------------------------------------------------+
| allow published  | ``!cb allow published``  | Toggles allowing published videos to be announced.             |
+------------------+--------------------------+----------------------------------------------------------------+
| allow greetings  | ``!cb allow greetings``  | Toggle allowing greetings when a member joins the server.      |
+------------------+--------------------------+----------------------------------------------------------------+
| allow goodbyes   | ``!cb allow goodbyes``   | Toggle allowing goodbyes when a member leaves the server.      |
+------------------+--------------------------+----------------------------------------------------------------+
| allow stats      | ``!cb allow stats``      | Toggle displaying stats such as "Total Views" and "Followers". |
+------------------+--------------------------+----------------------------------------------------------------+
| allow thumbnails | ``!cb allow thumbnails`` | Adds an image to the bottom of the embed from the stream.      |
+------------------+--------------------------+----------------------------------------------------------------+

.. note:: The "LiveDiscovery" feature requires a status of "Streaming" (Purple status colour).

**LiveDiscovery** is a feature of **CouchBot** that removes the need for manually added creators.
At the moment, this only works for **Twitch** and is triggered by the live streaming status on Discord.

+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
| Name                | Example                                    | Usage                                                                                               |
+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
| allow livediscovery | ``!cb allow livediscovery none``           | Prevents the automatic announcing of people with the status of "Streaming".                         |
+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
| allow livediscovery | ``!cb allow livediscovery role @Streamer`` | Allows the automatic announcing of people with the status of "Streaming" and the role of @Streamer. |
+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
| allow livediscovery | ``!cb allow livediscovery all``            | Allows the automatic announcing of people with the status of "Streaming".                           |
+---------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------+
