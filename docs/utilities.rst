.. _utilities:

==========
Utilities
==========

Use the following commands to configure various other bits and bobs.

+--------------------------+--------------------------------------+---------------------------------------------+
| Name                     | Example                              | Usage                                       |
+--------------------------+--------------------------------------+---------------------------------------------+
| info                     | ``!cb info``                         | Brief stats overview of the bot.            |
+--------------------------+--------------------------------------+---------------------------------------------+
| invite                   | ``!cb invite``                       | Sends you a message with the bot invite.    |
+--------------------------+--------------------------------------+---------------------------------------------+
| ytidlookup               | ``!cb ytidlookup "Channel Name"``    | Tries to locate the channel ID for YouTube. |
+--------------------------+--------------------------------------+---------------------------------------------+
| whatsthis                | ``!cb whatsthis``                    | Give a link to an image to find out.        |
+--------------------------+--------------------------------------+---------------------------------------------+
| config list              | ``!cb config list``                  | Shows the bot config list.                  |
+--------------------------+--------------------------------------+---------------------------------------------+
| config deleteoffline     | ``!cb config deleteoffline``         | Deletes offline streams.                    |
+--------------------------+--------------------------------------+---------------------------------------------+
| config textannouncements | ``!cb config textannouncements``     | Announces in text rather than embeds        |
+--------------------------+--------------------------------------+---------------------------------------------+
| clap                     | ``!cb clap place your text here``    | Places a clap in all the spaces.            |
+--------------------------+--------------------------------------+---------------------------------------------+
| cookies                  | ``!cb cookies place your text here`` | Places a cookie in all the spaces.          |
+--------------------------+--------------------------------------+---------------------------------------------+

-----------------------
Purge (Delete) Messages
-----------------------

Sometimes you may wish to clear channels, messages or just messages from specific people to help in the effective
moderation of you server. To accomplish this you can use the following commands.

.. Warning:: You must have **Manage Messages** permissions to run this command!
			 Once run, you will be unable to restore deleted messages.

+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| Name  | Example                                        | Usage                                                                                                          |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| purge | ``!cb purge``                                  | Deletes 100 messages in the current channel.                                                                   |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| purge | ``!cb purge 25``                               | This would purge 25 messages in the current channel.                                                           |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| purge | ``!cb purge 25 #DiscordChannel``               | This would purge 25 messages in the channel #DiscordChannel.                                                   |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| purge | ``!cb purge 25 #DiscordChannel true``          | This would purge 25 messages in the channel #DiscordChannel including pinned messages.                         |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| purge | ``!cb purge @MattTheDev``                      | Deletes all messages from user within the last 100 messages in the current channel.                            |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| purge | ``!cb purge @MattTheDev true``                 | Deletes all messages from user within the last 100 messages in the current channel, including pinned messages. |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| purge | ``!cb purge @MattTheDev #DiscordChannel``      | Deletes all messages from user within the last 100 messages in the current channel.                            |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
| purge | ``!cb purge @MattTheDev #DiscordChannel true`` | Deletes all messages from user within the last 100 messages in the current channel, including pinned messages. |
+-------+------------------------------------------------+----------------------------------------------------------------------------------------------------------------+
