.. _couchquick:

============================
CouchBot Install Guide
============================

In order to have CouchBot stay on your server, you will need to sign up to Patreon and whitelist your server.
To do this follow the guide below;

.. warning:: This must be done if you wish to use CouchBot!

1. `Sign up to one of the Patreon tiers! <https://patreon.com/CouchBot>`_
2. `Link your Patreon to your Discord account <https://www.patreon.com/settings/apps>`_
3. `Join the support server to start adding your servers <https://discord.couch.bot>`_
4. Add your Mixer, Picarto, Twitch or YouTube Channels:

.. code-block:: none

    !cb mixer YourMixerName #ChannelName
    !cb picarto YourPicartoName #ChannelName
    !cb twitch YourTwitchName #ChannelName
    !cb youtube YourYouTubeChannelID #ChannelName vod
    !cb youtube YourYouTubeChannelID #ChannelName live
    !cb youtube YourYouTubeChannelID #ChannelName both

.. note:: To get your YouTube Channel ID see `this guide <https://youtube.com/account_advanced>`_.
          It's 24 characters long and starts with UC.


5. Finally, tell **CouchBot** if its allowed to post live and published

.. code-block:: none

    !cb allow published (to allow/disallow published content)
    !cb allow live (to allow/disallow live content)

Done!

.. attention:: If you have any issues, please check the permissions assigned to the channel and bot.
